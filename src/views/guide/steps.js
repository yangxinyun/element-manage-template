const steps = [
  {
    element: '#hamburger-container',
    popover: {
      title: '收缩菜单',
      description: '点击展开和收缩菜单',
      position: 'bottom',
      nextBtnText: '下一个',
      prevBtnText: '上一个',
      closeBtnText: '关闭',
      doneBtnText: '完成'
    }
  },
  {
    element: '#breadcrumb-container',
    popover: {
      title: '面包屑',
      description: '指示当前页面位置',
      position: 'bottom',
      nextBtnText: '下一个',
      prevBtnText: '上一个',
      closeBtnText: '关闭',
      doneBtnText: '完成'
    }
  },
  {
    element: '#header-search',
    popover: {
      title: '页面搜索',
      description: '页面搜索，快速导航',
      position: 'left',
      nextBtnText: '下一个',
      prevBtnText: '上一个',
      closeBtnText: '关闭',
      doneBtnText: '完成'
    }
  },
  {
    element: '#screenfull',
    popover: {
      title: '全屏',
      description: '全屏展示页面内容',
      position: 'left',
      nextBtnText: '下一个',
      prevBtnText: '上一个',
      closeBtnText: '关闭',
      doneBtnText: '完成'
    }
  },
  {
    element: '#size-select',
    popover: {
      title: '字体大小',
      description: '改变全局字体大小',
      position: 'left',
      nextBtnText: '下一个',
      prevBtnText: '上一个',
      closeBtnText: '关闭',
      doneBtnText: '完成'
    }
  },
  {
    element: '#tags-view-container',
    popover: {
      title: '标签导航',
      description: '这里是你以前打开过的标签',
      position: 'bottom',
      nextBtnText: '下一个',
      prevBtnText: '上一个',
      closeBtnText: '关闭',
      doneBtnText: '完成'
    },
    padding: 0
  }
]

export default steps
